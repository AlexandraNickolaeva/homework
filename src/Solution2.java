import java.util.Arrays;
import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] m = new int[n];
        int[] w = new int[n];

        for(int i = 0; i<n; i++) {
            m[i] = input.nextInt();
            w[i] = (int)Math.pow(m[i],2);
        }
        Arrays.sort(w);
        for (int i = 0; i<n; i++) {
            System.out.print(w[i] + " ");
        }
    }
}
