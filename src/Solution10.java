import java.util.Scanner;

public class Solution10 {
    public static void main(String[] args) {
        game();
    }

    public static void game() {
        Scanner input = new Scanner(System.in);
        int m = (int) (Math.random() * 1001);
        int n = input.nextInt();

        while (n >= 0) {
            if (n == m) {
                System.out.println("Победа!");
                break;
            } else if (n < m)
                System.out.println("Это число меньше загаданного.");
            else
                System.out.println("Это число больше загаданного.");
            n = input.nextInt();
        }
    }
}
